###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
# 
# How the text input will look like:
# 
# 
# VLAN Name                             Status    Ports
# ---- -------------------------------- --------- -------------------------------
# 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
#                                                 Eth1/4
# 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
#                                                 Eth1/5, Eth1/16, Eth1/17
#                                                 Eth1/18, Eth1/49, Eth1/50
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################
import re
import json


def get_vlan_db(text):
    """
    This functions creates a dictionary, assigned on the returned variable, that containes in key value like pairs,
    the information of the SHOW VLAN command from a nexus device.
    :param text: text is a string that contains the output of the "show vlan" command.
    :return: Is a dictionary that contains all the information of the show vlan command, in the requested way
    """
    category = []
    vlan_db = {}
    counter = 0
    temp_name = ""
    for line in text.split("\n"):
        if counter == 0:
            for element in re.sub("\s+", " ", line).strip().split(" "):
                category.append(element)
        elif re.search("^-+", line[:4]):
            continue
        elif re.search("^[\s+]", line):
            splitted = re.sub("\s+", " ", line).strip().split(" ")
            for ports in splitted:
                vlan_db.get(temp_name).get(category[3]).append(ports)
        else:
            splitted = re.sub("\s+", " ", line).strip().split(" ")
            temp_name = splitted[0]
            vlan_db[splitted[0]] = {}
            vlan_db.get(splitted[0])[category[1]] = splitted[1]
            vlan_db.get(splitted[0])[category[2]] = splitted[2]
            vlan_db.get(splitted[0])[category[3]] = []
            for ports in splitted[3:]:
                vlan_db.get(splitted[0]).get(category[3]).append(ports)
        counter += 1
    return vlan_db


if __name__ == "__main__":
    text="""VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
                                                Eth1/4
10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
                                                Eth1/5, Eth1/16, Eth1/17
                                                Eth1/18, Eth1/49, Eth1/50"""
    dic = get_vlan_db(text)
    print(json.dumps(dic, indent=4))
