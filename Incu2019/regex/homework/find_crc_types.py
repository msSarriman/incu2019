###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the text input will look like:
#
# --------------------------------------------------------------------------------
# Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# Eth1/3                0          0          0          0          0           0
# Eth1/4                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
# --------------------------------------------------------------------------------
# Eth1/1             0          --           0           0           0          0
# Eth1/2             0          --           0           0           0          0
#
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################
import re
import json


def get_error_counters(text):
    """
    This function iterates through the text string, and returns a corresponding dictionary that contains sorted in a
    key value way, all the CRC information that exists inside the text.
    :param text: It is the crc response of a Nexus Switch
    :return: Returns a dictionary that contains as keys all the interfaces available in the crc text.
            The value of each key is a dictionary that contains the crc values.
    """
    crc_values = []
    dictionary = {}
    for line in text.split("\n"):
        if re.search(r"^(-+)+", line):
            continue
        elif re.search(r"^(<snip>)", line):
            continue
        elif re.search(r"^(Port)", line):
            if len(crc_values) != 0:
                crc_values = []
            for value in re.sub("\s+", " ", line).strip().split(" "):
                if value == "Port":
                    continue
                crc_values.append(value)
        else:
            temp = (re.sub("\s+", " ", line).strip().split(" "))
            if temp[0] not in dictionary:
                dictionary[temp[0]] = {}
            for index, values in enumerate(temp[1:]):
                if len(crc_values) == 0:
                    break
                (dictionary.get(temp[0]))[crc_values[index]] = values
    return dictionary


if __name__ == "__main__":
    nexus_result = """--------------------------------------------------------------------------------
Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
--------------------------------------------------------------------------------
Eth1/1                4          0          0          0          1           0
Eth1/2                0          0          2          0          0           0
Eth1/3                0          0          0          0          0           0
Eth1/4                0          0          0          0          0           0
<snip>
--------------------------------------------------------------------------------
Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
--------------------------------------------------------------------------------
Eth1/1                0          0          0          0          9           0
Eth1/2                0          0          0          0          0           0
<snip>
--------------------------------------------------------------------------------
Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
--------------------------------------------------------------------------------
Eth1/1                0          0          0          0          9           0
Eth1/2                0          0          0          0          0           0"""
    dic = get_error_counters(nexus_result)
    print(json.dumps(dic, indent=4))

