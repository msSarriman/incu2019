###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################
import re

def remove_duplicates(text):
    """
    This function finds the duplicates in the given "text" string and then removes them from it. It finally
    returns the string without the duplicates.
    :param text: The string to remove the duplicates from
    :return: The given string without the duplicates
    """
    temp_list = text.split()
    duplicate_list = []
    # First the algorithm finds all the duplicates in O(n^2)
    for word in temp_list:
        occurrence_counter = 0
        for it in re.finditer(word,text):
            occurrence_counter += 1
        if occurrence_counter > 1:
            duplicate_list.append(word)
    print("Duplicate words are "+str(set(duplicate_list)))
    # Then removes the duplicates from the string
    remove_indexes = []
    for duplicate in duplicate_list:
        occurrence_counter = 0
        for index, word in enumerate(temp_list):
            if duplicate == word:
                occurrence_counter += 1
                if occurrence_counter > 1:
                    temp_list.pop(temp_list.index(word))
    print("The corrected string is "+str(temp_list))
    return ' '.join(temp_list)


if __name__ == "__main__":
    text = """This is is some some simple text..
    With With duplicates."""
    print(remove_duplicates(text))
