###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################
import re

def is_valid_ip(ip):
    """
    This function tests if a given IP is valid.
    :param ip: The IP that is about to be tested.
    :return: Return True if False, if the IP is valid or invalid correspondingly.
    """
    if re.search(r"^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$", ip):
        return True
    else:
        return False

def get_ip_class(ip):
    """
    This function tests an ip for its class and return a string with the proper information.
    :param ip: This is an IP that is about to be tested for its class
    :return: Returns the class of ip, in string
    """
    if is_valid_ip(ip):
        print(re.search(r"^\b([0-9]|[1-9][0-9]|1[1-2][1-7])\b.", ip))
        if re.search(r"^\b([0-9]|[1-9][0-9]|1[1-2][1-7])\b.", ip):
            return str(ip)+" is a class A IP"
        elif re.search(r"^\b(12[89]|1[3-8][0-9]|19[01])\b.", ip):
            return str(ip)+" is a class B IP"
        elif re.search(r"^\b(19[2-9]|2[0-1][0-9]|22[0-3])\b.", ip):
            return str(ip)+" is a class C IP"
        elif re.search(r"^\b(22[4-9]|23[0-9])\b.", ip):
            return str(ip) + " is a class D IP"
        elif re.search(r"^\b(24[0-7])\b.", ip):
            return str(ip) + " is a class E IP"
        else:
            return str(ip)+" is classless IP"
    else:
        return str(ip)+" is not a valid IP address"

if __name__ == "__main__":
    print(is_valid_ip("192.168.2.1"))
    print(is_valid_ip("192.168.2.255"))
    print(get_ip_class("124.168.2.255"))
    print(get_ip_class("135.168.2.255"))
    print(get_ip_class("199.168.2.255"))
    print(get_ip_class("230.168.2.255"))
    print(get_ip_class("245.168.2.255"))
    print(get_ip_class("248.168.2.255"))
    print(get_ip_class("248.168.2.256"))

