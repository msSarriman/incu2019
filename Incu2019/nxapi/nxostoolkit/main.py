#!/usr/bin/python3
import requests
import json


class Nexus:
    """
    The class defines some basic methods to manipulate the Nexus 9000v using both
    CLI and REST API
    @__version__: stores the version the switch is running
    @__chassisid__ : stores the id of the switch chassis
    """
    __version__ = None
    __chassisid__ = None

    """
    __init__()
    @location: The location of the Nexus api (link or IP)
    @port: The port of the the nexus api
    If the @location is not set and takes the default "online" value,
    then there will be a reference to the online Cisco Nexus 9000v.
    Otherwise the given user input will be used.
    """
    def __init__(self, location = "online", port = None):
        self.session = requests.Session()
        self.username = None
        self.password = None
        self.headers = {'Content-Type': 'application/json'}
        if location == "online":
            self.url = "http://sbx-nxos-mgmt.cisco.com/"
        else:
            self.usr = "http://{}:{}/".format(location, port)

    """
    authenticate(username, password)
    @username: Username input
    @password: Password input
    This method authenticates some credentials and starts a session with the server
    """
    def authenticate(self, username, password):
        url = self.url + "api/aaaLogin.json"
        payload ={
                  "aaaUser": {
                    "attributes": {
                      "name": username,
                      "pwd": password
                    }
                  }
                }
        response = self.session.post(url, data=json.dumps(payload), headers=self.headers)
        if response.status_code == 200:
            # print("Credentials are correct!")
            self.username = username
            self.password = password
            self.load_version()
            self.load_chassis_id()
        elif response.status_code == 401:
            print("Credentials are not correct.")
        else:
            print("There was a connection error.")

    """
    get_interface_status(interface)
    @interface: the interface to be retrieved from the Nexus
    The method return information about the above given interface
    """
    def get_interface_status(self, interface):
        url = self.url + "ins"
        payload = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show interface " + str(interface),
                "output_format": "json"
            }
        }
        response = self.session.post(url,
                                     data=json.dumps(payload),
                                     headers=self.headers,
                                     auth=(self.username, self.password))
        print(response.text)

    """
    configure_interface_desc(interface, description)
    @interface: the interface to change the description
    @description: the description to be set to the above interface
    The method changes the default description of a given interface
    """
    def configure_interface_desc(self, interface, description):
        interface = "eth" + interface[-3:]
        url = self.url + "/api/node/mo/sys/intf/phys-["+interface+"].json"
        payload = {
            "l1PhysIf": {
                "attributes": {
                    "descr": description
                }
            }
        }
        response = self.session.post(url,
                                     data=json.dumps(payload),
                                     headers=self.headers,
                                     auth=(self.username, self.password))
        print(response.text)


    """
    logout()
    This method logouts the loged in user
    """
    def logout(self):
        url = self.url + "api/aaaLogout.json"
        payload = {
            "aaaUser": {
                "attributes": {
                    "name": self.username
                }
            }
        }
        self.session.post(url,
                          data=json.dumps(payload),
                          headers=self.headers,
                          auth=(self.username, self.password))

    """
    load_version()
    This method stores at the class variable __version__, the coresponding version
    of the Nexus
    """
    def load_version(self):
        url = self.url + "/ins"
        payload = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show version",
                "output_format": "json"
            }
        }
        response = json.loads(self.session.post(url,
                                                data=json.dumps(payload),
                                                headers=self.headers,
                                                auth=(self.username, self.password)
                                                ).text)
        __class__.version = response.get('ins_api')\
            .get("outputs")\
            .get("output")\
            .get("body")\
            .get("kickstart_ver_str")

    @staticmethod
    def get_version():
        return __class__.__version__

    """
    load_chassis_id()
    This method stores at the class variable __chassisid__, the corresponding chassisID
    that the Nexus is running at.
    """
    def load_chassis_id(self):
        url = self.url + "/ins"
        payload = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show version",
                "output_format": "json"
            }
        }
        response = json.loads(self.session.post(url,
                                                data=json.dumps(payload),
                                                headers=self.headers,
                                                auth=(self.username, self.password)
                                                ).text)
        __class__.__chassisid__ = response.get('ins_api')\
            .get("outputs")\
            .get("output")\
            .get("body")\
            .get("chassis_id")

    @staticmethod
    def get_chassis_id():
        return __class__.__chassisid__


if __name__ == "__main__":
    req = Nexus()
    req.authenticate("admin", "Admin_1234!")
    req.get_interface_status("Ethernet 1/1")
    req.configure_interface_desc("Ethernet1/1", "hello")
    req.get_version()
    req.get_chassis_id()
    req.logout()
