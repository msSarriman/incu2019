#!/usr/bin/python3
from flask import Flask
import json


app = Flask(__name__)
address_book = {}

"""
This route post an entry into the address book
"""
@app.route("/add/<userid>/<name>/<phone>/<email>", methods=['POST'])
def add_user(userid, name, phone, email):
    temp = add_entry(userid, name, phone, email)
    if temp == 'OK':
        return "User Added Successfully!"
    else:
        return "There was an error adding the user!\n" + temp


"""
This route returns the details for a user id
"""
@app.route("/get/<userid>")
def get_user(userid):
    temp = get_entry(userid)
    if temp != 'not found':
        return json.dumps(temp, indent=4)
    else:
        return "Not found!"


"""
This route displays all the information contained in the address book
"""
@app.route("/display_all")
def display_all():
    global address_book
    return json.dumps(address_book, indent=4)


"""
This route deletes the entry for a given id
"""
@app.route("/delete/<id>", methods=["DELETE"])
def del_id(id):
    global address_book
    if id in address_book:
        del address_book[id]
        return "ID deleted from address book."
    else:
        abort(404, "The id was not found")


"""
This function creates a new entry on the address book, given the specific details of 
id, name, phone, and email
"""
def add_entry(id, name, phone, email):
    global address_book
    try:
        address_book[id] = {}
        (address_book.get(id))["name"] = name
        (address_book.get(id))["phone"] = phone
        (address_book.get(id))["email"] = email
        return 'OK'
    except Exception as e:
        return e


"""
This function returns the details for a specific id in the address book
"""
def get_entry(myid):
    global address_book
    if myid in address_book:
        return address_book.get(myid)
    else:
        return "not found"


app.run(host="0.0.0.0",port=7676, debug=True)