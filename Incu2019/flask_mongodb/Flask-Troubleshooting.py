from flask import Flask
from flask import abort
from flask import Response
from flask import request
import json

app = Flask(__name__)


authorized_users = ['agata', 'tristan', 'peter', 'joanna']

@app.route("/hello/<username>")
def hello(username):
    return "Hello World %s !" % username

@app.route("/authorized_only/<usrname>")
def list_all(username):
    global authorized_users
    if username not in authorized_users:
        abort(500,'The user %s is not authorized to view the page' % usrname)
    else:
        return 'Welcome to your personal page, %s ' % (username)

app.run(host="0.0.0.0", port=7676)
